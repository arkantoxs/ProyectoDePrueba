/**
 * @author Miguel Angel
 *
 */
import javax.swing.JOptionPane;
public class EJ06 {

	public static void main(String[] args) {
		
		double IVA=0.21;
		
		String num = JOptionPane.showInputDialog("Ingresa el precio de un producto:");
		
		double n = Double.parseDouble(num);
		
		JOptionPane.showMessageDialog(null,"El precio final del producto con IVA �s de: "+(n+(n*IVA)));

	}

}
