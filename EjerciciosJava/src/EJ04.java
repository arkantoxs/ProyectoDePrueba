/**
 * @author Miguel Angel
 *
 */
import javax.swing.JOptionPane;
public class EJ04 {

	public static void main(String[] args) {
		
		JOptionPane.showMessageDialog(null,"Vamos a calcular el �rea de un circulo");
		
		String texto=JOptionPane.showInputDialog("Introduce un radio");
        
		//Pasamos el String a Double
        
		double r= Double.parseDouble(texto);
 
        //Formula para calcular el �rea del circulo
        
		double a=Math.PI*Math.pow(r, 2);
 
        JOptionPane.showMessageDialog(null,"El �rea del circulo �s:"+a);
		

	}

}
