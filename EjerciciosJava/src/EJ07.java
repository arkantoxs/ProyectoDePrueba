/**
 * @author Miguel Angel
 *
 */
public class EJ07 {

	public static void main(String[] args) {
		
		int num=1;
		 
        //BUCLE WHILE
		
        while (num<=100){
        	System.out.println(num);
            
            num++;
        }
    }
}