
package Principal;
import Ejercicios.Persona;
/**
 * @author Miguel �ngel
 *
 */
public class EJ01_main {

	public static void main(String[] args) {
		//Introduccion de datos
		
		Persona persona1=new Persona("Miguel Angel",20,48279267,"H",80.5,1.80);
		Persona persona2=new Persona("Antonio",47,48285678,"H",75.5,1.78);
		
		
		if(persona1.equals(persona2)){
			System.out.println("Son la misma persona");
		}else {
			System.out.println("Son personas diferentes");
		}
		
		Persona persona3;
		
		persona3=persona1;
		
		if(persona3.equals(persona1)){
			System.out.println("Son la misma persona");
		}else {
			System.out.println("Son personas diferentes");
		}
		
	

	}

}
   