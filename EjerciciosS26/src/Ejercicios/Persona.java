/**
 * 
 */
package Ejercicios;

/**
 * @author Miguel �ngel
 *
 */
public class Persona {
	
	
	//ATRIBUTOS--------------------------------------
	 
	
	//Nombre de persona
	public String nombre;
	
	//Edad de persona
	public int edad;
	
	//Dni de la persona
	public int DNI;
	
	//Sexo de la persona
	public String sexo;
	
	//Peso de la persona
	public double peso;
	
	//Altura de las persona
	public double altura;
	
	//CONSTRUCTORES-----------------------------------
	
	//POR DEFECTO 00
	public Persona(int DNI){
		this.nombre="";
		this.edad=0;
		this.DNI=DNI;
		this.sexo="H";
		this.peso=0;
		this.altura=0;
	 }
	//CONTRUCTOR 01
	
	public Persona (String nombre, int edad, String sexo){
		this.nombre=nombre;
		this.edad=edad;
		this.sexo=sexo;
	}
	//CONSTRUCTOR 02
	
	public Persona (String nombre, int edad, int DNI, String sexo, double peso, double altura){
		this.nombre=nombre;
		this.edad=edad;
		this.DNI=DNI;
		this.sexo=sexo;
		this.peso=peso;
		this.altura=altura;
		
	}
	
	//FIN DE CONSTRUCTORES------------------------------
	
	public int getedad(){
		return edad;
	}
	
		



}
