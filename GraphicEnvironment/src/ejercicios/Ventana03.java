package ejercicios;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import java.awt.Choice;

import javax.swing.ButtonGroup;
import javax.swing.JButton;

public class Ventana03 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana03 frame = new Ventana03();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana03() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//TITULOS
		JLabel lblEncuesta = DefaultComponentFactory.getInstance().createTitle("ENCUESTA");
		lblEncuesta.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEncuesta.setBounds(157, 11, 115, 25);
		contentPane.add(lblEncuesta);
		
		JLabel lblEligeUnSistema = DefaultComponentFactory.getInstance().createTitle("Elige un Sistema Operativo:");
		lblEligeUnSistema.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEligeUnSistema.setBounds(10, 42, 166, 14);
		contentPane.add(lblEligeUnSistema);
		
		JLabel lblElijeTuEspecialidad = DefaultComponentFactory.getInstance().createTitle("Elije tu especialidad:");
		lblElijeTuEspecialidad.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblElijeTuEspecialidad.setBounds(272, 42, 115, 14);
		contentPane.add(lblElijeTuEspecialidad);
		
		
		JLabel lblHoras = DefaultComponentFactory.getInstance().createTitle("Horas dedicadas al ordenador:");
		lblHoras.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHoras.setBounds(10, 145, 178, 14);
		contentPane.add(lblHoras);
		
		
		//JRADIO BUTTONS-----------------------------------------------------------------------------------
	
		final JRadioButton opcionWindows = new JRadioButton("Windows"); //Opcion1
		opcionWindows.setBounds(6, 63, 109, 23);
		contentPane.add(opcionWindows);
		
		final JRadioButton opcionLinux = new JRadioButton("Linux"); //Opcion2
		opcionLinux.setBounds(6, 89, 109, 23);
		contentPane.add(opcionLinux);
		
		final JRadioButton opcionMac = new JRadioButton("Mac"); //Opcion3
		opcionMac.setBounds(6, 115, 109, 23);
		contentPane.add(opcionMac);
		//Agrupamos los botones para que no sean independientes
		ButtonGroup opcionesSO=new ButtonGroup();
		opcionesSO.add(opcionWindows);
        opcionesSO.add(opcionLinux);
        opcionesSO.add(opcionMac);
        
        //CHECK BOXS-----------------------------------------------------------------------------------
		
		final JCheckBox opProg = new JCheckBox("Programaci\u00F3n");
		opProg.setBounds(272, 63, 115, 23);
		contentPane.add(opProg);
		
		final JCheckBox opDise�o = new JCheckBox("Dise\u00F1o Gr\u00E1fico");
		opDise�o.setBounds(272, 89, 115, 23);
		contentPane.add(opDise�o);
		
		final JCheckBox op3D = new JCheckBox("Modelado 3D");
		op3D.setBounds(272, 115, 115, 23);
		contentPane.add(op3D);
		//--------------------------------------------------------------------------------------------------
		
		JLabel lblnumero = DefaultComponentFactory.getInstance().createTitle("");
		lblnumero.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblnumero.setBounds(10, 130, 178, 14);
		contentPane.add(lblHoras);
		
		lblnumero.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblnumero.setText("0");
		
        //BOTON VALIDAR
		JButton btnValidar = new JButton("Validar");
		btnValidar.setBounds(10, 228, 89, 23);
		contentPane.add(btnValidar);
		
		//BOTON RESULTADO
		JButton btnResultado = new JButton("Resultado");
		btnResultado.setBounds(307, 228, 100, 23);
		contentPane.add(btnResultado);
		
		//SLIDER NRO HORAS AL ORDENADOR-------------------
		final JSlider slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(10, 170, 200, 47);
		slider.setMaximum(10);
		slider.setValue(0);
		slider.setMajorTickSpacing(1);
		
		contentPane.add(slider);
		//---------------------------------------------
		//LISTENER DEL SLIDER
		
		//LISTENER DEL BOTON VALIDAR
		btnValidar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Llamamos los textos de las opciones
				String w=opcionWindows.getText();
				String l=opcionLinux.getText();
				String m=opcionMac.getText();
				String p=opProg.getText();
				String d=opDise�o.getText();
				String tresd=op3D.getText();
				//--------------------------------------
				
				//SI ESCOGEMOS WINDOWS
				if(opcionWindows.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "El sistema Operativo escogido es: "+w);
				}
				//SI ESCOGEMOS LINUX
				if(opcionLinux.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "El sistema Operativo escogido es: "+l);
				}
				//SI ESCOGEMOS MAC
				if(opcionMac.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "El sistema Operativo escogido es: "+m);
					
				}
				//CheckBoxes
				//SI ESCOGEMOS PROGRAMACION
				if(opProg.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "Estas especializado en: "+p);
				}
				//SI ESCOGEMOS DISE�O GRAFICO
				if(opDise�o.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "Estas especializado en:"+d);
				}
				//SI MODELADO3D
				if(op3D.isSelected()){
					JOptionPane.showMessageDialog(contentPane, "Estas especializado en: "+tresd);
				}
				//SI SELECCIONAMOS EL NUMERO DE HORAS
				JOptionPane.showMessageDialog(contentPane, "Tus horas en el ordenador son: "+slider.getValue());
			
			}
			  			
		});
	}
	//METODO DEL EVENTO PARA EL BOTON "A�ADIR"----------------------------------------------------------------------------
	    
        
		
		
         
      
	    //DECLARACION DE VARIABLES
	    private javax.swing.JSlider slider;
	    private javax.swing.JTextArea setText;
}
