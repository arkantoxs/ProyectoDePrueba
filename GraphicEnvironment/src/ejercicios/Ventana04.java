package ejercicios;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Ventana04 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana04 frame = new Ventana04();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana04() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCalculadora = DefaultComponentFactory.getInstance().createTitle("CALCULADORA");
		lblCalculadora.setBounds(174, 11, 88, 14);
		contentPane.add(lblCalculadora);
		
		textField = new JTextField();
		textField.setBounds(150, 85, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(150, 158, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(291, 105, 86, 42);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Resultado");
		lblNewJgoodiesTitle.setBounds(304, 88, 64, 14);
		contentPane.add(lblNewJgoodiesTitle);
		
		JLabel lblN = DefaultComponentFactory.getInstance().createTitle("N1");
		lblN.setBounds(121, 88, 30, 14);
		contentPane.add(lblN);
		
		JLabel lblN_1 = DefaultComponentFactory.getInstance().createTitle("N2");
		lblN_1.setBounds(121, 161, 30, 14);
		contentPane.add(lblN_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(178, 116, 22, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblOp = DefaultComponentFactory.getInstance().createTitle("Op");
		lblOp.setBounds(150, 119, 30, 14);
		contentPane.add(lblOp);
		
		JButton button = new JButton("+");
		button.setBounds(38, 84, 41, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.setBounds(38, 123, 41, 23);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("/");
		button_2.setBounds(38, 157, 41, 23);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("*");
		button_3.setBounds(38, 191, 41, 23);
		contentPane.add(button_3);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(335, 228, 89, 23);
		contentPane.add(btnSalir);
	}
}
