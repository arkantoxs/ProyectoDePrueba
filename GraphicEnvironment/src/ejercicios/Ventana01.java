package ejercicios;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.TextField;
import javax.swing.SwingConstants;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;

public class Ventana01 extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana01 frame = new Ventana01();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana01() {
		setTitle("Saludador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(new Rectangle(108, 94, 207, 20));
		txtNombre.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombre.setText("Fernando");
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		
		//BOTON "SALUDAR"
		JButton btnSaludador = new JButton("\u00A1Saludar!");
		btnSaludador.setBounds(154, 134, 112, 23);
		contentPane.add(btnSaludador);
		//LISTENER DEL BOTON
		btnSaludador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	btnSaludadorActionPerformed(evt);
            }
        });
		
		JLabel lblScjhabcbghjasbc = DefaultComponentFactory.getInstance().createTitle("Escribe un nombre para saludar");
		lblScjhabcbghjasbc.setBounds(134, 59, 151, 14);
		contentPane.add(lblScjhabcbghjasbc);
		
		
	}
	 //METODO DEL EVENTO
         private void btnSaludadorActionPerformed(java.awt.event.ActionEvent evt) {                                             
        
          //Muestra el texto que esta en el textbox
        	 
          JOptionPane.showMessageDialog(this, "�Hola "+txtNombre.getText()+"!");
         
           }  
}
