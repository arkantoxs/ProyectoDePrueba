package ejercicios;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Ventana02 extends JFrame {
    
	private JPanel contentPane;
	private JTextField txtNombre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana02 frame = new Ventana02();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana02() {
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 488, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		campoPeliculas = new javax.swing.JComboBox();
		campoPeliculas.setBounds(247, 106, 151, 20);
		contentPane.add(campoPeliculas);
		
		txtNombre = new JTextField();
		txtNombre.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombre.setBounds(41, 106, 151, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblEscribeElTtulo = DefaultComponentFactory.getInstance().createTitle("Escribe el t\u00EDtulo de una pel\u00EDcula");
		lblEscribeElTtulo.setBounds(41, 75, 174, 14);
		contentPane.add(lblEscribeElTtulo);
		
		JLabel lblPelculas = DefaultComponentFactory.getInstance().createTitle("Pel\u00EDculas");
		lblPelculas.setBounds(301, 75, 53, 14);
		contentPane.add(lblPelculas);
		
		JButton btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.setBounds(77, 137, 89, 23);
		contentPane.add(btnAnadir);
		
		//LISTENER DEL BOTON A�ADIR
		btnAnadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	btnAnadirActionPerformed(evt);
            }
        });
		
	}
	    //METODO DEL EVENTO PARA EL BOTON "A�ADIR"----------------------------------------------------------------------------
        private void btnAnadirActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
        //Cogemos el texto del TEXTFIELD con un GET
        	
        String pelicula=txtNombre.getText();
         
        //A�adimos la pelicula al COMBOBOX llamado "campoPeliculas"
        
        campoPeliculas.addItem(pelicula);
         
        //Si queremos vaciar el campo de texto usamos:
        // txtNombre.setText("");
         //-------------------------------------------------------------------------------------------------------------------
    } 
        
      //DECLARACION DE VARIABLES
        private javax.swing.JComboBox campoPeliculas;
        private javax.swing.JTextField txtPelicula;
}
