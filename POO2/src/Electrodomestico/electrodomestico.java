/**
 * 
 */
package Electrodomestico;

import javax.swing.JOptionPane;

/**
 * @author Usuario
 *
 */
public class electrodomestico {
	
	//ATRIBUTOS

	private double precioBase;
	
	private String color;
	
	private String consumoEnergetico;
	
	private double peso;
	
	
	//CONSTRUCTOR POR DEFECTO
	public electrodomestico() {
		this.precioBase = 100;
		this.color = "Blanco";
		this.consumoEnergetico = "F";
		this.peso = 5;
	}
	//CONSTRUCTOR CON TODOS LOS VALORES
	public electrodomestico(double precioBase, String color, String consumoEnergetico, double peso) {
		
		this.precioBase = precioBase;
		comprobarConsumEnergetico(consumoEnergetico);
		comprobarColor(color);
		this.peso = peso;
		
		
	}
	//CONSTRUCTOR PRECIOBASE, PESO
	public electrodomestico(double precioBase,  double peso) {
		this.precioBase = precioBase;
		this.color = "Blanco";
		this.consumoEnergetico = "F";
		this.peso = peso;
	}
	//GETS Y SETS

	public double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public void setConsumoEnergetico(String consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	//----------------------------------------------------------------------

	//METODO PARA COMPROBAR CONSUMO ENERGETICO
	public void comprobarConsumEnergetico( String consumoEnergetico)
	{
		if(consumoEnergetico.equalsIgnoreCase("A") || consumoEnergetico.equalsIgnoreCase("B") || consumoEnergetico.equalsIgnoreCase("C") || consumoEnergetico.equalsIgnoreCase("D") ||consumoEnergetico.equalsIgnoreCase("E") || consumoEnergetico.equalsIgnoreCase("F")){
			this.consumoEnergetico = consumoEnergetico;
		}else{
			this.consumoEnergetico = "F";
		}
	}
		
	//METODO PARA COMPROBAR COLOR
	
	
	public void comprobarColor( String color)
	{
		if(color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("gris")){
			this.color = color;
		}else{
			this.color = "blanco";
		}
	
	//METODO DEL PRECIO FINAL
	}
	
	public double precioFinal ()
	{
		double precioFinal=0;
	
		switch (consumoEnergetico) {
		 
        case "A":
        precioFinal=precioBase+100;
        break;
 
        case "B":
        precioFinal=precioBase+80;
        break;
        
        case "C":
        precioFinal=precioBase+60;
        break;
        
        case "D":
        precioFinal=precioBase+50;
        break;
        
        case "E":
        precioFinal=precioBase+30;
        break;
        
        case "F":
        precioFinal=precioBase+10;
        break;
        
      
        default:
        JOptionPane.showMessageDialog(null, "La letra digitada no existe");
        break;
 
       }
		
    
	    if(peso<=19)
	    {
	    	precioFinal+=10;
	    }
	    if(peso>=20 && peso<=49)
	    {
	    	precioFinal+=50;
	    }
	    if(peso>=50 && peso<=79)
	    {
	    	precioFinal+=80;
	    }
	    if(peso>=80)
	    {
	    	precioFinal+=100;
	    }
	    
	    return precioFinal;
	}

}
