/**
 * 
 */
package MainApp;
import Electrodomestico.electrodomestico;
import Subclase.Lavadora;
import Subclase.Television;

/**
 * @author Equipo3
 *
 */
public class MainElectromesticoApp {
	
	public static void main(String[] args) {
		
		//OBJETO//
		electrodomestico electrodomestic = new electrodomestico(); 
	    
		//ARRAYS DE OBJETOS
		electrodomestico arrayObjetos[]=new electrodomestico[10];
		arrayObjetos[0]=new electrodomestico();
		arrayObjetos[1]=new electrodomestico(150,"Negro","F",6);
		arrayObjetos[2]=new electrodomestico(250,"Rojo","A",8);
		arrayObjetos[3]=new electrodomestico(270,"Azul","C",8);
		arrayObjetos[4]=new electrodomestico(180,"Gris","B",7);
		arrayObjetos[5]=new electrodomestico(180,"Rojo","D",7);
		arrayObjetos[6]=new electrodomestico(190,"Blanco","E",6);
		arrayObjetos[7]=new electrodomestico(350,"Negro","F",5);
		arrayObjetos[8]=new electrodomestico(450,"Azul","A",7);
		arrayObjetos[9]=new electrodomestico(650,"Rojo","B",8);
		
		System.out.println("ELECTRODOMESTICOS\n");
		
		//MOSTRAMOS EN PANTALLA LOS PRECIOS BASE Y FINAL DE LOS ELECTRODOMESTICOS
		for (int i=0;i<arrayObjetos.length;i++){
		 double pantalla=arrayObjetos[i].getPrecioBase();
		 
		 System.out.println("Electrodomestico "+i+" Precio Base: "+pantalla+"");
		 System.out.println("Precio Final: "+arrayObjetos[i].precioFinal());
		}
	}
}
