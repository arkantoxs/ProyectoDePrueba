/**
 * 
 */
package Subclase;

import Electrodomestico.electrodomestico;

/**
 * @author Usuario
 *
 */
public class Television extends electrodomestico{
	
	//ATRIBUTOS
	private boolean sintonizador;
	private int resolucion;
	
	//CONSTRUCTORES
	public Television()
	{
		super();
		this.sintonizador=false;
		this.resolucion=20;
	}
	
	public Television(double precioBase, double peso)
	{
		super(precioBase,peso);
		this.sintonizador=false;
		this.resolucion=20;
	}
	
	public Television (double peso, String color, String consumoEnergetico, double precioBase, int carga, int resolucion, boolean sintonizador)
	{
    	super (peso,color,consumoEnergetico,precioBase);
    	this.sintonizador=sintonizador;
    	this.resolucion=resolucion;

	}
	//-------------------------------------------------------------------
	
	//GETS Y SETS

         
	public boolean isSintonizador() {
		return sintonizador;
	}

	public void setSintonizador(boolean sintonizador) {
		this.sintonizador = sintonizador;
	}

	public int getResolucion() {
		return resolucion;
	}

	public void setResolucion(int resolucion) {
		this.resolucion = resolucion;
	
	}
	//----------------------------------------------------
	
	//METODO PRECIO FINAL
	
	public double precioFinal(int resolucion){
		
		double 	Pb = super.precioFinal();

		
		if(resolucion>40){
			Pb=(Pb*30)/100;
		    
		}
		
		if (sintonizador=true){
			Pb+=50;
		}
		return Pb;
		
	}

}
