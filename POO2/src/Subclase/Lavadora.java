/**
 * 
 */
package Subclase;

import Electrodomestico.electrodomestico;

/**
 * @author Usuario
 *
 */
public class Lavadora extends electrodomestico {
	
	private int carga;

    
    //CONSTRUCTOR POR DEFECTO
    public Lavadora(){
    	super();
    	this.carga=5;
		
	}
    //CONSTRUCTOR DE PRECIOBASE Y PESO
    public Lavadora(double precioBase, double peso){
		super(precioBase,peso);
		this.carga=5;
	}
    //CONSTRUCTOR CARGA Y EL RESTO
    public Lavadora(double peso, String color, String consumoEnergetico, double precioBase, int carga){
    	super (peso,color,consumoEnergetico,precioBase);
    	this.carga=carga;
    	
    //GETS
    }
	public int getCarga() {
		return carga;
	}
	public void setCarga(int carga) {
		this.carga = carga;
	}
	//----------------------------------------------
	
	//METODO PRECIO FINAL
	public double precioFinal(int carga){
		
		double Pb=0;
		
		if(carga>30){
			Pb = super.precioFinal(); //Llamar al m�todo
			Pb+=50;
		    
		}
		return Pb;
		
	}
    
}
