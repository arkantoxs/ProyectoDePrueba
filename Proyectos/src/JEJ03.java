/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class JEJ03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//DECLARACI�N DE VARIABLES
		int X=7, Y=2;
		double N=8.5, M=6.4;
		
		//OPERACIONES CON VARIABLES
		int SUMA1=(X+Y);
		int RESTA1=(X-Y);
		int PRODUCTO1=(X*Y);
		int DIVISION1=(X/Y);
		int RESTO1=(X&Y);
		
		double SUMA2=(N+M);
		double RESTA2=(N-M);
		double PRODUCTO2=(N*M);
		double DIVISION2=(N/M);
		double RESTO2=(N%M);
		
		double SUMA3=(X+N);
		double DIVISION3=(Y/M);
		double RESTO3=(Y%M);
		
		int DX=(X*2);
		int DY=(Y*2);
		double DN=(N*2);
		double DM=(M*2);
		
		double SUMAT=(X+Y+N+M);
		double PRODUCTOT=(X*Y*N*M);
		
		//IMPRESI�N EN PANTALLA
		System.out.println("El valor de X �s: "+X);
		System.out.println("El valor de Y �s: "+Y);
		System.out.println("El valor de N �s: "+N);
		System.out.println("El valor de M �s: "+M);
		
		System.out.println("\nLa suma de X+Y �s: "+SUMA1);
		System.out.println("La resta de X-Y �s: "+RESTA1);
		System.out.println("El producto de X*Y �s: "+PRODUCTO1);
		System.out.println("El cociente de X/Y �s: "+DIVISION1);
		System.out.println("El resto de X%Y �s: "+RESTO1);
		
		System.out.println("\nLa suma de N+M �s: "+SUMA2);
		System.out.println("La resta de N-M �s: "+RESTA2);
		System.out.println("El producto de N*M �s: "+PRODUCTO2);
		System.out.println("El cociente de N/M �s: "+DIVISION2);
		System.out.println("El resto de N%M �s: "+RESTO2);
		
		System.out.println("\nLa suma de X+M �s: "+SUMA3);
		System.out.println("El cociente de Y/M �s: "+DIVISION3);
		System.out.println("El resto de Y%M �s: "+RESTO3);
		
		System.out.println("\nEl valor doble de X �s: "+DX);
		System.out.println("El valor doble de Y �s: "+DY);
		System.out.println("El valor doble de N �s: "+DN);
		System.out.println("El valor doble de M �s: "+DM);
		
		System.out.println("\nLa suma de todas las variables �s: "+SUMAT);
		System.out.println("\nEl producto de todas las variables �s: "+PRODUCTOT);
		
		System.out.println("\nFIN DEL PROGRAMA");
		
		
		

	}

}
