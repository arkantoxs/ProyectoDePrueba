/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class PruebaApp2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int precioProducto=300;
		final double IVA=0.21;
		
		System.out.println("Informaci�n del producto\n");
		System.out.println("El precio del producto �s: "+precioProducto);
		System.out.println("El precio del producto, incluyendo el IVA, �s: "+(precioProducto+(precioProducto*IVA)));

	}

}
