/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class JEJ05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//DECLARACI�N DE VARIABLES
		int A=2, B=4, C=6, D=8;
		//Valores
		
		
		//IMPRESI�N EN PANTALLA
		System.out.println("El valor inicial de A �s: "+A);
		System.out.println("El valor inicial de B �s: "+B);
		System.out.println("El valor inicial de C �s: "+C);
		System.out.println("El valor inicial de D �s: "+D);
		
		System.out.println("\nB ahora toma el valor de C siendo este: "+(B=C));
		System.out.println("\nC ahora toma el valor de A siendo este: "+(C=A));
		System.out.println("\nA ahora toma el valor de D siendo este: "+(A=D));
		System.out.println("\nD ahora toma el valor de B siendo este: "+(D=B));
		
		System.out.println("\nFIN DEL PROGRAMA");

	}

}
