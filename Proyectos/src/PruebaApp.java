/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class PruebaApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int op1=10;
		int op2=20;
		int op3=10;
		
		boolean result;
		
		result = op1==10 && op2<30;
		//�Es 10 igual a 10 y es 20 menor que 30? resultado= true
		System.out.println("�Es 10 igual a 10 y es 20 menor que 30? "+result);
		
		result = op1!=30 || op3<15;
		//�Es 10 distinto a 30 y es 10 igual a 10? resultado= true
		System.out.println("\n�Es 10 distinto a 30 y es 10 igual a 10? "+result);
		
		result = op1==op3 && op2<30 || op2>=15;
		//�Es 10 igual que 10 y es 20 menor que 30 o es 10 mayor o igual que 15? resultado=true
		//Aqui primero se evalua las condiciones con && y el resultado de esta se compara con ||
		System.out.println("\n�Es 10 igual que 10 y es 20 menor que 30 o es 10 mayor o igual que 15? "+result);
	}

}
