/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class PruebaApp01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int operador1 =10;
		int operador2 =20;
		int operador3 =10;
		
		boolean resultado;
		
		resultado = operador1==operador2;  // �10 es igual a 20? resultado = false
		System.out.println("�10 es igual a 20? "+resultado);
		
		resultado = operador1==operador3;   // �10 es igual a 10? resultado = true
		System.out.println("�10 es igual a 10? "+resultado);
		
		resultado = operador1!=operador2;  // �10 es distinto 20? resultado = false
		System.out.println("�10 es distinto 20? "+resultado);
		
		resultado = operador1>operador2;  // �10 es mayor que 20? resultado = false
		System.out.println("�10 es mayor que 20? "+resultado);
		
		resultado = operador1>=operador3;   // �10 es mayor que 10? resultado = false (Si no cumple la condicion ser� falso)
		System.out.println("�10 es mayor que 10? "+resultado);
		
		resultado = operador1<operador2;  // �10 es menor que 20? resultado = true
		System.out.println("�10 es menor que 20? "+resultado);
		
		resultado = operador1<=operador3;   // �10 es menor o igual que 10? resultado = true (ya que es igual)
		System.out.println("�10 es menor o igual que 10? "+resultado); 
		
		resultado = operador1>=operador2;   // �10 es mayor o igual que 20? resultado = false
		System.out.println("�10 es mayor o igual que 20? "+resultado);
	}

}
