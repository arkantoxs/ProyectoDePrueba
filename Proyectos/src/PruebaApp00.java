/**
 * 
 */

/**
 * @author MiguelAngel
 * @version 1.0
 * @date 27/05/2016
 */
public class PruebaApp00 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int operador1 =20;
		int operador2 =15;
		int resultado =0;
		
		resultado= operador1+operador2; //resultado=35
		System.out.println("operador1+operador2 "+resultado);
		
		resultado= operador1-operador2; //resultado=5
		System.out.println("operador1-operador2 "+resultado);
		
		resultado= operador2-operador1; //resultado=-5
		System.out.println("operador2-operador1 "+resultado);
		
		resultado= operador1*operador2; //resultado=300
		System.out.println("operador1*operador2 "+resultado);
		
		resultado= operador1/operador2; //resultado=1 (como es int no incluye decimales)
		System.out.println("operador1/operador2 "+resultado);
		
		resultado= operador1%operador2; //resultado=5 (el resto de la divisi�n)
		System.out.println("operador1�%operador2 "+resultado);
	}

}
