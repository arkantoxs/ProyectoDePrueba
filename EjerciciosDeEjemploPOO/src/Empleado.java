/**
 * 
 */

/**
 * @author Miguel �ngel
 *
 */

/*
 * Clase Empleado
 * 
 * Contiene informaci�n de cada empleado
 * 
 */
public class Empleado {
	//Atributos de la clase Empleado
	
	/*
	 * Nombre del empleado
	 */
	private String nombre;
	/*
	 * Apellido del empleado
	 */
	private String apellido;
	/*
	 * Edad del empleado
	 */
	private int edad;
	/*
	 * Salario del empleado
	 */
	private double salario;
	
	//METODOS
	/*
	 * Suma un plus al salario del empleado si el empleado tiene mas de 
	 * 40 a�os @param sueldoPlus
	 */
	public boolean plus(double sueldoPlus){
		boolean aumento=false;
		 if (edad>40){
		   salario+=sueldoPlus;
		        aumento=true;
		 }
	            return aumento;
	}
	/** --------------------------------------------------------------
	 * SOBRECARGA DE M�TODOS
	 * 
	 1ra funci�n o m�todo
	 ----------------------
	public boolean plus(double sueldoPlus){
		boolean aumento=false;
		if (edad>40 && compruebaNombre()){
			salario+=sueldoPlus;
			aumento=true;
		}
		return aumento;
	}
	
	 2nda funci�n
	 
	 public void plus(double sueldoPlus){
	    
	    if (edad>40 && compruebaNombre()){
	           salario+=sueldoPlus
	    }
	 
	 }
	*/
	
	//SOBRECARGA  DE CONSTRUCTORES
	
	/*
	  public Empleado(){
	      this ("","",0, 4);
	  }
	  
	  public Empleado(String nombre, String apellido){
		  this (nombre,apellido,0,4);
	  }
	 */
	
	// CONSTRUCTORES
	/**
	 * Constructor por defecto
	 */
	/*public Empleado(){
		this.nombre="";
		this.apellido="";
		this.edad=0;
		this.salario=0;
	}*/
	/**
	 * Constructor con 4 parametros
	 * @param nombre --nombre del empleado
	 * @param apellido --apellido del empleado
	 * @param edad --edad del empleado
	 * @param salario --salario del empleado
	 */
	public Empleado(String nombre, String apellido, int edad, double salario){
		this.nombre=nombre;
		this.apellido=apellido;
		this.edad=edad;
		this.salario=salario;
	}
}
