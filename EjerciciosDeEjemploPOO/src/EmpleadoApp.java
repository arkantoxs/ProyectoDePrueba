/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
public class EmpleadoApp {

	public static void main(String[] args) {
		/**
		 * Sintaxis para crear un objeto de una clase:
		 * 
		 * nombre_clase nombre_objeto=new nombre_clase(parametros_constructor);
		 * 
		 * En este caso ser�a as�:
		 */
		
		Empleado empleado1=new Empleado ("Fernando","Ure�a",23, 800);
		//Devolvera false
		System.out.println(empleado1.plus(30));
		
		//Creamos un array de objetos de la clase empleados
		Empleado arrayObjetos[]=new Empleado[3];
		
		//Creamos objetos en cada posicion
		arrayObjetos[0]=new Empleado("Fernando", "Ure�a", 23,1000);
		arrayObjetos[1]=new Empleado("Epi", "Dermis",30, 1500);
		arrayObjetos[2]=new Empleado("Blas", "Femia", 25, 1200);
		
		//Recorremos el array para calcular la suma de salarios
		/*
		int suma=0;
		
		for (int i=0;i<arrayObjetos.length;i++){
			suma+=arrayObjetos[i].getSalario();
		}
		System.out.println("La suma de salarios es"+suma);*/
	}

}
