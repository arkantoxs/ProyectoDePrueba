import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
public class EJ05 {

	public static void main(String[] args) {
		//De base decimal a binario
		
		//Pedimos un n�mero
		
		String t= JOptionPane.showInputDialog("Ingresa un n�mero: ");
		int n = Integer.parseInt(t);
		
		String binario=decimalAbinario(n);
		JOptionPane.showMessageDialog(null, "El numero "+n+ " en binario es "+binario);
        

	}
	
	public static String decimalAbinario (int numero){
        String binario="";
        String digito;
        for(int i=numero;i>0;i/=2){
            if(i%2==1){
                digito="1";
            }else{
                digito="0";
            }
            
            binario=digito+binario;
        }
        return binario;
    }

}
