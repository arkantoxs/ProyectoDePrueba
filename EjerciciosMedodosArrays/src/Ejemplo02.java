/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
public class Ejemplo02 {

	public static void main(String[] args) {
		
		int op1=3;
		int op2=5;
		
		if (sumaNumeros(op1, op2)){
			System.out.println("El resultado es mayor que 0");
		}else{
			System.out.println("El resultado es menor que 0");
		}

	}
	
	public static boolean sumaNumeros (int n1, int n2){
		int resultado=n1+n2;
		
		if(resultado>=0){
			return true;
		}else{
			return false;
		}
	}

}
