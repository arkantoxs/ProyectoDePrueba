/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
import javax.swing.JOptionPane;
public class EJ03 {

	public static void main(String[] args) {
		//Pedimos un n�mero por teclado
		String t= JOptionPane.showInputDialog("Ingresa un n�mero por teclado: ");
		//Pasamos el string a entero
		int num=Integer.parseInt(t);
        //Ejecutamos la funcion declarada"
        if (esPrimo(num)){
        	JOptionPane.showMessageDialog(null, "El numero "+num+" es primo");
        }else{
        	JOptionPane.showMessageDialog(null, "El numero "+num+" no es primo");
        }
    }
    public static boolean esPrimo (int num){
 
        //Si el n�mero es menor o igual que 1 no �s primo
        if (num<=1){
            return false;
        }
 
        int cont=0;
        // Var divisor int------=--raiz de num---divisor >1; disminuir divisor
        for (int divisor=(int)Math.sqrt(num);divisor>1;divisor--){ //Math.sqrt --Raiz cuadrada
            //Comprueba los divisores
        	//Si el residuo de la division entre el n�mero y el divisor
                if (num%divisor==0){
                cont+=1;
            }
        }
        //Seg�n el numero de divisibles es o no primo"
        if (cont>=1){
            return false;
        }else{
            return true;
        }
    }
}
