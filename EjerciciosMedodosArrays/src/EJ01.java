/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
import javax.swing.JOptionPane;
public class EJ01 {


	public static void main(String[] args) {
		
        //DECLARO ESTA VARIABLE PARA REUTILIZARLA DESPUES
        String t="";
        
        //VARIABLE PARA MOSTRAR EL RESULTADO
        double resultado=0;
        String opcion=JOptionPane.showInputDialog("Escoge una figura: circulo, triangulo o cuadrado");
        switch (opcion){
        
        //OPCIONES A ESCOGER DE ACUERDO A LA FIGURA
        case "circulo":
            t=JOptionPane.showInputDialog("Introduce el radio");
            int radio=Integer.parseInt(t);
            resultado=areaCirculo(radio);
            break;
        case "triangulo":
            t=JOptionPane.showInputDialog("Introduce la base");
            int base=Integer.parseInt(t);
            t=JOptionPane.showInputDialog("Introduce la altura");
            int altura=Integer.parseInt(t);
            resultado=areaTriangulo(base, altura);
            break;
        case "cuadrado":
            t=JOptionPane.showInputDialog("Introduce un lado");
            int lado=Integer.parseInt(t);
            resultado=areaCuadrado(lado);
            break;
        default:
            JOptionPane.showMessageDialog(null, "Figura introducida no reconocida");
        }
        JOptionPane.showMessageDialog(null, "El area del "+opcion+" es "+resultado);
        //System.out.println("El area del "+opcion+" es "+resultado);
   }
   public static double areaCirculo (int radio){
        return Math.pow(radio, 2)*Math.PI;
    }
   public static double areaTriangulo (int base, int altura){
        return base*altura/2;
    }
   public static double areaCuadrado (int lado){
        return lado*lado;
    }
}
