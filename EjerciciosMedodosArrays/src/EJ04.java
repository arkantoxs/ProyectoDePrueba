import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Miguel Angel
 *
 */
public class EJ04 {

	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "EL FACTORIAL DE UN N�MERO");
        String t=JOptionPane.showInputDialog("Introduce un numero");
        int num=Integer.parseInt(t);
        JOptionPane.showMessageDialog(null, "El factorial de "+num+ " es " +factorial(num));
    }
   //Funci�n para calcular el factorial del numero digitado
	
	public static int factorial (int numero){
        int res=numero;
       
        for(int cont=(numero-1);cont>0;cont--){
            
            res=res*cont;
        }
        return res;
    }
}
