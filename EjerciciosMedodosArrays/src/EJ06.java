import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Miguel �ngel
 *
 */
public class EJ06 {

	
	public static void main(String[] args) {
		//PEDIMOS NUMERO POR VENTANA
        int num=0;
           
        do{
            String t=JOptionPane.showInputDialog("Introduce un n�mero");
            num=Integer.parseInt(t);
        }while(num<0);
        //El numero de cifras ser� igual al n�mero de cifras que el contador haya encontrado
        int numCifras=cuentaCifras(num);
        //CONTROLAMOS
        if (numCifras==1){
        	JOptionPane.showMessageDialog(null, "El numero "+num+ " tiene "+numCifras+" cifra");
        }else{
        	JOptionPane.showMessageDialog(null, "El numero "+num+ " tiene "+numCifras+" cifras");
        }
    }
    //CONTADOR DE CIFRAS
	public static int cuentaCifras (int num){
         int contador=0;
         for (int i=num;i>0;i/=10){
             contador++;
         }
         return contador;
    }
}
