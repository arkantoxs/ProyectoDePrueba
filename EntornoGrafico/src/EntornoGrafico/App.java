/**
 * 
 */
package EntornoGrafico;
import java.awt.TextField;

import javax.swing.*;
/**
 * @author Miguel Angel
 *
 */
public class App extends JFrame{
	//ATRIBUTOS
	
	private JPanel contentPane; //Solo se usa en esta clase

	
	//CONSTRUCTOR POR DEFECTO
	public App()
	{
		
		//A�adimos Titulo
		setTitle("Titulo de la ventana");
		
		//Coordenadas "X" y "Y" de la aplicaci�n y su altura y longitud
		
		setBounds(400,200,400,500);
		
		//Indicamos que cuando se cierre la ventana, la aplicaci�n se acaba
		setDefaultCloseOperation(EXIT_ON_CLOSE); //Salir al cerrar
		
		//Hace visible la ventana al usuario
		
		setVisible(true);
		
		//Creamos el panel
		contentPane =new JPanel();
		
		//Indicamos su dise�o
		contentPane.setLayout(null);
		
		//Asignamos el panel a la ventana
		setContentPane(contentPane);
		
		//CREACION DE LOS COMPONENTES
		//------------------------------------------------------
		//ETIQUETA
		JLabel etiqueta=new JLabel("�Hola People!");
		//Colocamos el componente
		etiqueta.setBounds(60,20,100,20);
		//A�adimos el elemento creado al panel para que se vea
		contentPane.add(etiqueta);
		
		
		//CAMPO DE TEXTO
	    JTextField CampoTexto = new JTextField("Ingrese su nombre");
		CampoTexto.setBounds(43,67,86,26);
		contentPane.add(CampoTexto);
		
		//BOTON
	    JButton Boton = new JButton("Click");
	    Boton.setBounds(43,133,89,23);
	    contentPane.add(Boton);
	    
	    //BOTON RADIO
	    JRadioButton BotonRadio1 = new JRadioButton("Opcion1", true);//OPCION1
	    BotonRadio1.setBounds(43,194,109,23);
	    contentPane.add(BotonRadio1);
	    
	    JRadioButton BotonRadio2 = new JRadioButton("Opcion2", false);//OPCION2
	    BotonRadio2.setBounds(43,220,109,23);
	    contentPane.add(BotonRadio2);
	    
	    JRadioButton BotonRadio3 = new JRadioButton("Opcion3", false);//OPCION3
	    BotonRadio3.setBounds(43,246,109,23);
	    contentPane.add(BotonRadio3);
	    
	    ButtonGroup GrupoBoton = new ButtonGroup();
	    GrupoBoton.add(BotonRadio1);
	    GrupoBoton.add(BotonRadio2);
	    GrupoBoton.add(BotonRadio3);
	    
	    //BOTON CHECKBOX
	    
	    JCheckBox OpcionCheck1 = new JCheckBox("Opcion1", true);//OPCION1
	    OpcionCheck1.setBounds(43,305,97,23);
	    contentPane.add(OpcionCheck1);
	    
	    JCheckBox OpcionCheck2 = new JCheckBox("Opcion2", true);//OPCION2
	    OpcionCheck2.setBounds(43,325,97,23);
	    contentPane.add(OpcionCheck2);
	    
	    JCheckBox OpcionCheck3 = new JCheckBox("Opcion2", true);//OPCION3
	    OpcionCheck3.setBounds(43,346,97,23);
	    contentPane.add(OpcionCheck3);
	    
	    //TEXTO DE AREA
	    
	    JTextArea AreaTexto = new JTextArea("Hola mi nombre es Miguel Angel Tautiva Quintero, tengo 20 a�os y me gusta programar y aprender cosas nuevas, y, ahora mismo estoy probando este scroll o barra de desplazamiento");
	    AreaTexto.setBounds(189,18,141,117);
	    AreaTexto.setWrapStyleWord(true);
	    AreaTexto.setLineWrap(true);
	    contentPane.add(AreaTexto);
	    
	    //SCROLL
	    JScrollPane desplazamiento = new JScrollPane(AreaTexto); //OBJETO
	    desplazamiento.setBounds(189,18,141,117); //las mismas coordenadas que el objeto anterior
	    contentPane.add(desplazamiento);
	    
	    //CAMPO DE PASSWORD
	    
	    JPasswordField CampoContrase�a = new JPasswordField("Steven");
	    CampoContrase�a.setBounds(189,171,139,20);
	    contentPane.add(CampoContrase�a);
	    
	    //MENU DE SELECCION
	    
	    JComboBox comboBox = new JComboBox<>();
	    comboBox.setBounds(189,221,141,22);
	    contentPane.add(comboBox);
	    
	    comboBox.addItem("Steven Yeun");
	    comboBox.addItem("Norman Reedus");
	    comboBox.addItem("Andrew Lincoln");
	    
	    //INTERRUPTOR
	    
	    JToggleButton Interruptor = new JToggleButton("Interruptor", true);
	    Interruptor.setBounds(189,291,121,23);
	    contentPane.add(Interruptor);
	    
	    //JSPINNER ----SIRVE PARA LLEVAR UN CONTADOR
	    
	    JSpinner spinner = new JSpinner();
	    spinner.setBounds(371,20,29,20);
	    contentPane.add(spinner);
	    
	    //LISTA----RECOMENDABLE INDICAR EL TIPO DE DATO A  ALMACENAR
	    String Peliculas[] ={"El Hobbit: Un viaje inesperado","El Hobbit: La desolaci�n de Smaug","El Hobbit: La batalla de los cinco ejercitos","El Se�or de los Anillos: La comunidad del Anillo","El Se�or de los Anillos: Las dos Torres","El Se�or de los Anillos: El retorno del Rey"};
	    
	    JList lista = new JList<>(Peliculas);
	    lista.setBounds(371,72,300,80);
	    contentPane.add(lista);
	    
	    JScrollPane scrollLista = new JScrollPane(lista); //OBJETO
	    scrollLista.setBounds(371,72,300,80); //las mismas coordenadas que el objeto anterior
	    contentPane.add(scrollLista);
	    
	    
	    //MENUS
	    //Creamos la barra
	    
	    JMenu Archivo= new JMenu("Archivo");
	    JMenu Editar = new JMenu("Editar");
	    JMenuBar barraMenu = new JMenuBar();
	    
	    
	    
		//PARAMETROS
	    barraMenu.add(Archivo);
		barraMenu.add(Editar);
	    
	    JMenuItem Abrir= new JMenuItem("Abrir");
	    JMenuItem Guardar= new JMenuItem("Guardar");
	    JMenuItem Cargar= new JMenuItem("Cargar");
	    JMenuItem Salir= new JMenuItem("Salir");
	    
	    JMenuItem Modificar= new JMenuItem("Modificar");
	    JMenuItem Copiar= new JMenuItem("Copiar");
	    
	    //CREAMOS LOS SUBMENUS
	    Archivo.add(Abrir);
	    Archivo.add(Guardar);
	    Archivo.add(Cargar);
	    Archivo.add(Salir);
	    
	    Editar.add(Modificar);
	    Editar.add(Copiar);
	    
	    //INDICAMOS QUE VAMOS A USAR LA BARRA
	    
	    setJMenuBar(barraMenu);
	    
	    
	 }
	 
	    

}






